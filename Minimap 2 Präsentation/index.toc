\babel@toc {ngerman}{}
\beamer@sectionintoc {1}{Grundlagen}{3}{0}{1}
\beamer@sectionintoc {2}{Methodik}{10}{0}{2}
\beamer@subsectionintoc {2}{1}{Ketten}{11}{0}{2}
\beamer@subsectionintoc {2}{2}{Gap Penalties}{27}{0}{2}
\beamer@subsectionintoc {2}{3}{Suzuki Kasahara Formulation}{38}{0}{2}
\beamer@subsectionintoc {2}{4}{Banded Dynamic Programming}{43}{0}{2}
\beamer@subsectionintoc {2}{5}{Heuristiken}{49}{0}{2}
\beamer@sectionintoc {3}{Ergebnisse}{55}{0}{3}
